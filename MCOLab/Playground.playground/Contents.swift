//: Playground - noun: a place where people can play

import Cocoa
import MailCore
import PlaygroundSupport
PlaygroundPage.current.needsIndefiniteExecution = true

var str = "Hello, playground"

protocol Command {
    func execute()
}
/*
class SearchFromYahoo: Command {
    func execute() {
        let session = MCOIMAPSession()
        session.username = "xuan.thanh8395@yahoo.com"
        session.hostname = "imap.mail.yahoo.com"
        session.password = "hh123467890"
        session.port = 993
        session.connectionType = .TLS
        session.authType = MCOAuthType(rawValue:0)
        
        session.connectionLogger = { (connectionID, type, data: Data?) in
            if let data = data {
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    debugPrint("\(#line) search message...: \(string)")
                }
            }
        }
        
        let op: MCOIMAPSearchOperation? = session.searchOperation(withFolder: "INBOX",
                                                                      kind: .kindFrom, search: "bi")
        
        op?.start { (error: Error?, indexSet: MCOIndexSet?) in
            debugPrint("search")
            debugPrint(error)
            debugPrint(indexSet)
        }
    }
}

class SearchFromGoogle: Command {
    func execute() {
        let session = MCOIMAPSession()
        session.username = "lebaotrung93.pr@gmail.com"
        session.hostname = "imap.gmail.com"
        session.oAuth2Token = "ya29.GlvgBTnqbsJxQcZ2Nu-eWbkit1l0ywRG8rDcKeHNNsb7vnv_jVEAB93CU3gRplhdUVsTlfgTT8vZtHwH_wAOC-w_3Gnlety9fnEU_wuUuxx6OIzJCPuXfRuACNGP"
        session.port = 993
        session.connectionType = .TLS
        session.authType = .init(rawValue: 256)
        
        session.connectionLogger = { (connectionID, type, data: Data?) in
            if let data = data {
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    debugPrint("\(#line) search message...: \(string)")
                }
            }
        }
        
        let op: MCOIMAPSearchOperation? = session.searchOperation(withFolder: "INBOX",
                                                                  kind: .kindFrom, search: "no")
        
        op?.start { (error: Error?, indexSet: MCOIndexSet?) in
            debugPrint("search")
            debugPrint(error)
            debugPrint(indexSet)
        }
    }
}

let cmd: Command = SearchFromGoogle()
cmd.execute()
*/

class FloLeech: Command {
    func execute() {
        let session = MCOIMAPSession()
        session.username = "transformer@123flo.com"
        session.hostname = "mail.123flo.com"
        session.password = "111111"
        session.port = 143
        session.connectionType = .startTLS
        session.authType = .init(rawValue: 0)
        
//        session.connectionLogger = { (connectionID, type, data: Data?) in
//            if let data = data {
//                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
//                    debugPrint("\(#line) search message...: \(string)")
//                }
//            }
//        }
        
        let uids: MCOIndexSet = MCOIndexSet(range: MCORange(location: 2, length: 1))
        debugPrint(uids)
        let op = session.fetchMessagesOperation(withFolder: "INBOX", requestKind: .headers, uids: uids)
        
        op?.start({ (error, any, set) in
            debugPrint(error)
            debugPrint(any)
            debugPrint(set)
        })
    }
}

let cmd: Command = FloLeech()
cmd.execute()
